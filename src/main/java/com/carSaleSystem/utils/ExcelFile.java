package com.carSaleSystem.utils;

import com.carSaleSystem.entity.Customer;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

@Service
public class ExcelFile {

    public void generateExcelFile(List<Customer> customers) throws IOException {
        Workbook workbook = new XSSFWorkbook();

        Sheet sheet = workbook.createSheet("Customers");
        sheet.setColumnWidth(0, 3000);
        sheet.setColumnWidth(0, 3000);
        sheet.setColumnWidth(0, 3000);
        sheet.setColumnWidth(0, 3000);
        sheet.setColumnWidth(0, 3000);

        Row header = sheet.createRow(0);

        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        XSSFFont font = ((XSSFWorkbook) workbook).createFont();
        font.setFontName("Arial");
        font.setFontHeight((short) 18);
        font.setBold(true);
        headerStyle.setFont(font);

        Cell headerCell = header.createCell(0);
        headerCell.setCellValue("Name");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(1);
        headerCell.setCellValue("Address");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(2);
        headerCell.setCellValue("Phone Number");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(3);
        headerCell.setCellValue("Email Address");
        headerCell.setCellStyle(headerStyle);

        //writing the content of the table with a different style
        CellStyle style = workbook.createCellStyle();
        style.setWrapText(true);

//        Row row = sheet.createRow(4);
//        for(Customer customer: customers){
//            Cell cell = row.createCell(0);
//            cell.setCellValue(customer.getName());
//            cell.setCellStyle(style);
//
//            cell = row.createCell(1);
//            cell.setCellValue(customer.getAddress());
//            cell.setCellStyle(style);
//
//            cell = row.createCell(2);
//            cell.setCellValue(customer.getPhoneNumber());
//            cell.setCellStyle(style);
//
//            cell = row.createCell(3);
//            cell.setCellValue(customer.getEmailAddress());
//            cell.setCellStyle(style);
//
//        }


        Iterator<Customer> iterator = customers.iterator();

        int rowIndex = 1;
        while(iterator.hasNext()){
            Customer customer = iterator.next();
            Row row = sheet.createRow(rowIndex++);
            Cell cell0 = row.createCell(0);
            cell0.setCellValue(customer.getName());
            Cell cell1 = row.createCell(1);
            cell1.setCellValue(customer.getAddress());
            Cell cell2 = row.createCell(2);
            cell2.setCellValue(customer.getPhoneNumber());
            Cell cell3 = row.createCell(3);
            cell3.setCellValue(customer.getPhoneNumber());
        }
        //writing content to temp.xlsx file and then closing the workbook
        File currDir = new File(".");
        String path = currDir.getAbsolutePath();
        String fileLocation = path.substring(0, path.length() - 1) + "temp.xlsx";

        FileOutputStream outputStream = new FileOutputStream(fileLocation);
        workbook.write(outputStream);
        workbook.close();

    }
}
