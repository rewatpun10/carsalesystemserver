package com.carSaleSystem.service;

import com.carSaleSystem.entity.NewCar;

import java.util.List;

public interface NewCarService {
    NewCar createNewCar(NewCar customer);

    NewCar updateNewCar(Long id, NewCar customer);

    void deleteNewCarById(Long id);

    NewCar getNewCarById(Long customerId);

    List<NewCar> getAllNewCar();
}
