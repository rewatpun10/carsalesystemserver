package com.carSaleSystem.service;

import com.carSaleSystem.entity.Customer;
import com.itextpdf.text.DocumentException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface CustomerService {

    Customer createCustomer(Customer customer);

    Customer updateCustomer(Long id, Customer customer);

    void deleteCustomerById(Long id);

    Customer getCustomerById(Long customerId);

    List<Customer> getAllCustomer();

    void downloadCustomers() throws IOException;


    void generateIndividualPdf(Long id);
}
