package com.carSaleSystem.controller;

import com.carSaleSystem.entity.Customer;
import com.carSaleSystem.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

//Basically handles request from REST api for Customer
@CrossOrigin
@RestController
public class CustomerController {

    //Injecting CustomerService using autowired
    @Autowired
    public CustomerService customerService;

    @CrossOrigin
    @PostMapping("/customers")
    public ResponseEntity<Customer>  createCustomer(@RequestBody Customer customer){
    return  ResponseEntity.ok().body(this.customerService.createCustomer(customer));
    }

    @GetMapping("customers/{id}")
    public ResponseEntity<Customer> getCustomerById(@PathVariable Long id){
        Customer customer = customerService.getCustomerById(id);
        return ResponseEntity.ok().body(customer);
    }

    @GetMapping("customers")
    public ResponseEntity<List<Customer>> getAllCustomers(){
        return ResponseEntity.ok().body(customerService.getAllCustomer());
    }

    @DeleteMapping("customers/{id}")
    public HttpStatus deleteCustomer(@PathVariable Long id){
            this.customerService.deleteCustomerById(id);
            return HttpStatus.OK;
    }

    @CrossOrigin
    @PutMapping("customers/{id}")
    public ResponseEntity<Customer>  updateCustomer(@PathVariable Long id, @RequestBody Customer customer){
        return  ResponseEntity.ok().body(this.customerService.updateCustomer(id, customer));
    }

    @GetMapping("downloadCustomers")
    public void downloadCustomers() throws IOException {
        customerService.downloadCustomers();
    }

    @GetMapping("/customers/generateIndividualReport/{id}")
    public void generateIndividualPdf(@PathVariable Long id) throws IOException {
        customerService.generateIndividualPdf(id);
    }
}
