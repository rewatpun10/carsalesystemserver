package com.carSaleSystem.controller;

import com.carSaleSystem.entity.NewCar;
import com.carSaleSystem.service.NewCarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//Basically handles request from REST api for Customer
@CrossOrigin
@RestController
public class NewCarController {

    @Autowired
    public NewCarService newCarService;


    @CrossOrigin
    @PostMapping("/newCars")
    public ResponseEntity<NewCar> createNewCars(@RequestBody NewCar newCar){
        return  ResponseEntity.ok().body(this.newCarService.createNewCar(newCar));
    }

    @GetMapping("/newCars")
    public ResponseEntity<List<NewCar>> getAllCustomers(){
        return ResponseEntity.ok().body(this.newCarService.getAllNewCar());
    }

}
