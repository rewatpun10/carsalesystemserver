package com.carSaleSystem.repository;

import com.carSaleSystem.entity.NewCar;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NewCarRepository extends JpaRepository<NewCar, Long> {
}
