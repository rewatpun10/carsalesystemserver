package com.carSaleSystem.serviceImpl;

import com.carSaleSystem.entity.Customer;
import com.carSaleSystem.exception.ResourceNotFoundException;
import com.carSaleSystem.repository.CustomerRepository;
import com.carSaleSystem.service.CustomerService;
import com.carSaleSystem.utils.ExcelFile;
import com.carSaleSystem.utils.HeaderFooter;
import com.carSaleSystem.utils.PdfCreator;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfPageEvent;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import javax.transaction.Transactional;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

    //Injecting CustomerRepository using autowired
    @Autowired
    public CustomerRepository customerRepository;

    @Autowired
    public ExcelFile excelFile;

    @Autowired
    public HeaderFooter headerFooter;

    @Autowired
    public PdfCreator pdfCreator;

    @Override
    public Customer createCustomer(Customer customer) {
        return customerRepository.save(customer);
    }

    @Override
    public Customer updateCustomer(Long id, Customer customer) {
        Optional<Customer> customerDB = this.customerRepository.findById(id);
        if(customerDB.isPresent()){
            Customer customerUpdate = customerDB.get();
            customerUpdate.setId(id);
            customerUpdate.setName(customer.getName());
            customerUpdate.setAddress(customer.getAddress());
            customerUpdate.setPhoneNumber(customer.getPhoneNumber());
            customerUpdate.setEmailAddress(customer.getEmailAddress());
            customerRepository.save(customerUpdate);
            return customerUpdate;
        }else {
            throw new ResourceNotFoundException(" Record not found with id: " + customer.getId());
        }
    }

    @Override
    public void deleteCustomerById(Long id) {
        Optional<Customer> customerDB = this.customerRepository.findById(id);
        if(customerDB.isPresent()){
            this.customerRepository.delete(customerDB.get());
        }  else {
            throw new ResourceNotFoundException("Record not found with id : " + id);
        }
    }

    @Override
    public Customer getCustomerById(Long customerId) {
        Optional<Customer> customer = this.customerRepository.findById(customerId);
        if(customer.isPresent()){
            return customer.get();
        }else {
            throw new ResourceNotFoundException("Customer not found with id: " +customerId);
        }
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.findAll();
    }

    @Override
    public void downloadCustomers() throws IOException {
        List<Customer> customerList = customerRepository.findAll();
        excelFile.generateExcelFile(customerList);

    }

    @Override
    public void generateIndividualPdf(Long id){
         String TITLE = "TestReport";
         String PDF_EXTENSION = ".pdf";
        Optional<Customer> customer = customerRepository.findById(id);
        if(customer.isPresent()){
            Document document = null;
            try{
                document = new Document(PageSize.A4);

                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(new File(TITLE + PDF_EXTENSION)));
                document.open();

                headerFooter.setHeader(" Individual Customer Report");
//                writer.setPageEvent((PdfPageEvent) headerFooter);



                PdfCreator.addMetaData(document, TITLE);

                PdfCreator.addTitlePage(document, TITLE);

                PdfCreator.addContent(document, customer.get());

            } catch (DocumentException | FileNotFoundException e) {

                e.printStackTrace();

                System.out.println("FileNotFoundException occurs.." + e.getMessage());

            }finally{

                if(null != document){
                    document.close();
                }
            }

        }else {
            throw new ResourceNotFoundException("Customer not found with id: " +id);
        }

    }
}
