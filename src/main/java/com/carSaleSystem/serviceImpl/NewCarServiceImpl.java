package com.carSaleSystem.serviceImpl;

import com.carSaleSystem.entity.NewCar;
import com.carSaleSystem.repository.NewCarRepository;
import com.carSaleSystem.service.NewCarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class NewCarServiceImpl implements NewCarService {

    @Autowired
    public NewCarRepository newCarRepository;

    @Override
    public NewCar createNewCar(NewCar newCar) {
        return newCarRepository.save(newCar);
    }

    @Override
    public NewCar updateNewCar(Long id, NewCar customer) {
        return null;
    }

    @Override
    public void deleteNewCarById(Long id) {

    }

    @Override
    public NewCar getNewCarById(Long customerId) {
        return null;
    }

    @Override
    public List<NewCar> getAllNewCar() {
        return newCarRepository.findAll();
    }
}
