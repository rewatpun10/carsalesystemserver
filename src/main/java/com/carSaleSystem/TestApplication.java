package com.carSaleSystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties
@SpringBootApplication
//@EntityScan(basePackages = {"com.carSaleSystem.entity"})  // scan JPA entities
public class TestApplication {

	public static void main(String[] args)  {
		SpringApplication.run(TestApplication.class, args);
	}

}
