package com.carSaleSystem.entity;

import javax.persistence.*;
import java.io.Serializable;

//It represents common properties of car that are inherited by new car and used car class
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String referenceNumber;
    private String make;
    private String model;
    private String driveType;
    private String colour;
    private String transmission;
    private String carEngine;
    private String fuelType;
    private String numberOfDoors;
    private String numberOfSeats;
    private float price;
    private int numberOfCars;

    //parameterized constructor
    public Product(String referenceNumber, String make, String model, String driveType, String colour, String transmission, String carEngine, String fuelType, String numberOfDoors, String numberOfSeats, float price, int numberOfCars) {
        this.referenceNumber = referenceNumber;
        this.make = make;
        this.model = model;
        this.driveType = driveType;
        this.colour = colour;
        this.transmission = transmission;
        this.carEngine = carEngine;
        this.fuelType = fuelType;
        this.numberOfDoors = numberOfDoors;
        this.numberOfSeats = numberOfSeats;
        this.price = price;
        this.numberOfCars = numberOfCars;
    }

    //default constructor
    public Product() {
    }


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getDriveType() {
        return driveType;
    }

    public void setDriveType(String driveType) {
        this.driveType = driveType;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getTransmission() {
        return transmission;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public String getCarEngine() {
        return carEngine;
    }

    public void setCarEngine(String carEngine) {
        this.carEngine = carEngine;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getNumberOfDoors() {
        return numberOfDoors;
    }

    public void setNumberOfDoors(String numberOfDoors) {
        this.numberOfDoors = numberOfDoors;
    }

    public String getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(String numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getNumberOfCars() {
        return numberOfCars;
    }

    public void setNumberOfCars(int numberOfCars) {
        this.numberOfCars = numberOfCars;
    }
}
