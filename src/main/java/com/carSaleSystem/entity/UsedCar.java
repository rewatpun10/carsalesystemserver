package com.carSaleSystem.entity;

//Represents used car attributes

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class UsedCar extends Product implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private int odometer;
    private String regoNo;
    private String regoEpiry;
    private String serviceHistory;
    private String vehicleIdentificationNumber;
    private String carHistory;

    //parameterized constructor
    public UsedCar(String referenceNumber, String make, String model, String driveType, String colour, String transmission, String carEngine, String fuelType, String numberOfDoors, String numberOfSeats, float price, int numberOfCars, int odometer, String regoNo, String regoEpiry, String serviceHistory, String vehicleIdentificationNumber, String carHistory) {
        super(referenceNumber, make, model, driveType, colour, transmission, carEngine, fuelType, numberOfDoors, numberOfSeats, price, numberOfCars);
        this.odometer = odometer;
        this.regoNo = regoNo;
        this.regoEpiry = regoEpiry;
        this.serviceHistory = serviceHistory;
        this.vehicleIdentificationNumber = vehicleIdentificationNumber;
        this.carHistory = carHistory;
    }

    public UsedCar() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public int getOdometer() {
        return odometer;
    }

    public void setOdometer(int odometer) {
        this.odometer = odometer;
    }

    public String getRegoNo() {
        return regoNo;
    }

    public void setRegoNo(String regoNo) {
        this.regoNo = regoNo;
    }

    public String getRegoEpiry() {
        return regoEpiry;
    }

    public void setRegoEpiry(String regoEpiry) {
        this.regoEpiry = regoEpiry;
    }

    public String getServiceHistory() {
        return serviceHistory;
    }

    public void setServiceHistory(String serviceHistory) {
        this.serviceHistory = serviceHistory;
    }

    public String getVehicleIdentificationNumber() {
        return vehicleIdentificationNumber;
    }

    public void setVehicleIdentificationNumber(String vehicleIdentificationNumber) {
        this.vehicleIdentificationNumber = vehicleIdentificationNumber;
    }

    public String getCarHistory() {
        return carHistory;
    }

    public void setCarHistory(String carHistory) {
        this.carHistory = carHistory;
    }
}
