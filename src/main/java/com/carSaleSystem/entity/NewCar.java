package com.carSaleSystem.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

//Represents new car attributes
@Entity
public class NewCar extends Product implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private int warranty;
    private int extendingWarranty;
    private String roadSideAssistancePackages;

    //parameterized constructor
    public NewCar(String referenceNumber, String make, String model, String driveType, String colour, String transmission, String carEngine, String fuelType, String numberOfDoors, String numberOfSeats, float price, int numberOfCars, int warranty, int extendingWarranty, String roadSideAssistancePackages) {
        super(referenceNumber, make, model, driveType, colour, transmission, carEngine, fuelType, numberOfDoors, numberOfSeats, price, numberOfCars);
        this.warranty = warranty;
        this.extendingWarranty = extendingWarranty;
        this.roadSideAssistancePackages = roadSideAssistancePackages;
    }

    //default constructor
    public NewCar() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public int getWarranty() {
        return warranty;
    }

    public void setWarranty(int warranty) {
        this.warranty = warranty;
    }

    public int getExtendingWarranty() {
        return extendingWarranty;
    }

    public void setExtendingWarranty(int extendingWarranty) {
        this.extendingWarranty = extendingWarranty;
    }

    public String getRoadSideAssistancePackages() {
        return roadSideAssistancePackages;
    }

    public void setRoadSideAssistancePackages(String roadSideAssistancePackages) {
        this.roadSideAssistancePackages = roadSideAssistancePackages;
    }
}
